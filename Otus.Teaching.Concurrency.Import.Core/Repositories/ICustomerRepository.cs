using Otus.Teaching.Concurrency.Import.Core.Entities;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core.Repositories
{
    public interface ICustomerRepository
    {
        void AddCustomer(Customer customer);
        void AddCustomers(IEnumerable<Customer> customers);
        void SaveChanges();

    }

    public interface ICustomerRepositoryAsync
    {
        Task AddCustomerAsync(Customer customer);
        Task SaveChangesAsync();
        Task AddCustomersAsync(IEnumerable<Customer> customers);

    }
}