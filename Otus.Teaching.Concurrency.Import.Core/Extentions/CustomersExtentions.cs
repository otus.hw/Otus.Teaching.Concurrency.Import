﻿using Otus.Teaching.Concurrency.Import.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Core.Extentions
{

    public static class CustomersExtentions
    {
        public static IEnumerable<IEnumerable<Customer>> ToChunk(this IEnumerable<Customer> source,
                int size)
        {
            using (var iter = source.GetEnumerator())
            {
                while (iter.MoveNext())
                {
                    var chunk = new Customer[size];
                    int count = 1;
                    chunk[0] = iter.Current;
                    for (int i = 1; i < size && iter.MoveNext(); i++)
                    {
                        chunk[i] = iter.Current;
                        count++;
                    }
                    if (count < size)
                    {
                        Array.Resize(ref chunk, count);
                    }
                    yield return chunk;
                }
            }
        }

        public static int GetChunkSize(this List<Customer> source, int threadPool)
        {
            int chunkSize;
            Console.WriteLine($"Start Load");
            if ((source.Count % threadPool) == 0)
            {
                chunkSize = source.Count / threadPool;
            }
            else
            {
                chunkSize = source.Count / threadPool + 1;
            }
            return chunkSize;
        }
        


    }
}

