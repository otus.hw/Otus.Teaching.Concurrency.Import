using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository, ICustomerRepositoryAsync
    {
        private readonly DataContext _dataContext;

        public CustomerRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void AddCustomer(Customer customer)
        {
            _dataContext.Customers.Add(customer);
        }

        public void SaveChanges()
        {
            _dataContext.SaveChanges();
        }


        public void AddCustomers(IEnumerable<Customer> customers)
        {
            _dataContext.AddRange(customers);
        }

        public async Task AddCustomerAsync(Customer customer)
        {
            await _dataContext.Customers.AddAsync(customer);
        }
        public async Task SaveChangesAsync()
        {
            await _dataContext.SaveChangesAsync();
        }

        public async Task AddCustomersAsync(IEnumerable<Customer> customers)
        {
            await _dataContext.AddRangeAsync(customers);
        }
    }
}