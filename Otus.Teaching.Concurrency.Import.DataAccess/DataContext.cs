﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        private static string  _connectionString;

        public DataContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public DataContext()
        {

        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (_connectionString.Contains("postgres"))
            {
                optionsBuilder.UseNpgsql(_connectionString);
            }
            else
            {
                optionsBuilder.UseSqlite("Filename=Customers.Sqlite");
            }
        }
    }
}
