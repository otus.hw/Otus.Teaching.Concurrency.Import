﻿using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using System.IO;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser : IDataParser<List<Customer>>
    {
        string _filePath;
        public XmlParser(string filePath)
        {
            _filePath = filePath;
        }
        public List<Customer> Parse()
        {
            //Parse data
            using var stream = new StreamReader(_filePath);
            var customersList = (CustomersList)new XmlSerializer(typeof(CustomersList)).Deserialize(stream);
            return customersList.Customers;
        }
    }
}