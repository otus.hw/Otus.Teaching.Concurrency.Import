﻿using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Extentions;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Loader.Handlers;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class DataLoader : IDataLoader
    {
        List<Customer> _customers;
        int _threadPool;
        int _batchSize;
        int _attemptsSaveToDB;

        public DataLoader(List<Customer> customers, int threadPool, int butchSize, int attemptsSaveToDB)
        {
            _customers = customers;
            _threadPool = threadPool;
            _batchSize = butchSize;
            _attemptsSaveToDB = attemptsSaveToDB;
        }



        public void LoadData()
        {
            var chunkSize = _customers.GetChunkSize(_threadPool);
            var threads = new List<Thread>();

            var customerChunks = _customers.ToChunk(chunkSize);

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            foreach (var chunk in customerChunks)
            {
                var loader = new LoaderHandler(chunk, _batchSize, _attemptsSaveToDB);
                var thread = new Thread(loader.Handle);
                thread.Start();
                threads.Add(thread) ;
            }

            foreach (var thread in threads)
            {
                thread.Join();
            }
            stopwatch.Stop();
            Console.WriteLine($"Time to load to DB: {stopwatch.Elapsed} ");
        }

        public async Task LoadDataAsync()
        {
            var chunkSize = _customers.GetChunkSize(_threadPool);

            var tasks = new List<Task>();

            var customerChunks = _customers.ToChunk(chunkSize);

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            foreach (var chunk in customerChunks)
            {
                var loaderAsync = new LoaderHandlerAsync(chunk, _batchSize, _attemptsSaveToDB);
                var task = Task.Run(() => loaderAsync.Handle());
                tasks.Add(task);
            }

            await Task.WhenAll(tasks);

            stopwatch.Stop();
            Console.WriteLine($"Time to load async to DB: {stopwatch.Elapsed} ");
        }
    }
}
