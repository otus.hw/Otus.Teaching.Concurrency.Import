﻿using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Extentions;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader.Handlers
{
    public class LoaderHandler
    {
        int _batchSize;
        int _attemptsSaveToDB;
        private readonly IEnumerable<Customer> _customers;

        public LoaderHandler(IEnumerable<Customer> customers, int batchSize, int attemptsSaveToDB)
        {
            _batchSize = batchSize;
            _attemptsSaveToDB = attemptsSaveToDB;
            _customers = customers;
        }

        public void Handle()
        {
            Console.WriteLine($"Handler thread with id {Thread.CurrentThread.ManagedThreadId} started loading {_customers.Count()} items...");

            var chunks = _customers.ToChunk(_batchSize);

            foreach (var item in chunks)
            {
                using (var dataContext = new DataContext())
                {
                    var repository = new CustomerRepository(dataContext);
                    repository.AddCustomers(item);
                    TrySaveChanges(repository);
                }
            }
        }

        private void TrySaveChanges(CustomerRepository repository)
        {
            int tryCounter = 0;
            do
            {
                try
                {
                    repository.SaveChanges();
                    break;
                }
                catch (Exception exc)
                {
                    Console.WriteLine(exc);
                    tryCounter++;
                }
            }
            while (tryCounter < _attemptsSaveToDB);

        }


    }
}
