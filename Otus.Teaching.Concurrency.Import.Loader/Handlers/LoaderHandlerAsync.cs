﻿using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Extentions;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader.Handlers
{
    public class LoaderHandlerAsync
    {
        int _batchSize;
        int _attemptsSaveToDB;
        private readonly IEnumerable<Customer> _customers;

        public LoaderHandlerAsync(IEnumerable<Customer> customers, int batchSize, int attemptsSaveToDB)
        {
            _batchSize = batchSize;
            _attemptsSaveToDB = attemptsSaveToDB;
            _customers = customers;
        }

        public async Task Handle()
        {
            Console.WriteLine($"Handler thread with id {Thread.CurrentThread.ManagedThreadId} started loading {_customers.Count()} items...");

            var chunks = _customers.ToChunk(_batchSize);

            foreach (var item in chunks)
            {
                using (var dataContext = new DataContext())
                {
                    var repository = new CustomerRepository(dataContext);
                    await repository.AddCustomersAsync(item);
                    await TrySaveChangesAsync(repository);
                }
            }
        }
        private async Task TrySaveChangesAsync(CustomerRepository repository)
        {
            int tryCounter = 0;
            do
            {
                try
                {
                    await repository.SaveChangesAsync();
                    break;
                }
                catch (Exception exc)
                {
                    Console.WriteLine($"Db Lock {tryCounter}");
                    tryCounter++;
                }
            }
            while (tryCounter < _attemptsSaveToDB);

            if (tryCounter == _attemptsSaveToDB)
            {
                Console.WriteLine($"Saving to Db failed");
            }

        }

    }
}
