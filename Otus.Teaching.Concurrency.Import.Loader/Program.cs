﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");
        private static string _dataFileFullPath = _dataFilePath;
        private static string _generatorFilePath ;
        private static int _dataCount = 3_000_000;
        
        static async Task Main(string[] args)
        {
            IConfiguration Configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json", optional: true, reloadOnChange: true).Build();
            var threadPool = Configuration.GetValue<int>("ThreadPool");
            var batchSize = Configuration.GetValue<int>("BatchSize");
            var attemptsSaveToDB = Configuration.GetValue<int>("AttemptsSaveToDB");
            string connection = Configuration.GetConnectionString("DefaultConnection");



            ParseArgs(args);
            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            if (string.IsNullOrEmpty(_generatorFilePath))
            {
                GenerateCustomersDataFile();
            }
            else
            {
                var process =  Process.Start(_generatorFilePath, "customers "+_dataCount.ToString());
                _dataFileFullPath = Path.Combine(Path.GetDirectoryName(_generatorFilePath), "customers.xml");
                process.WaitForExit();
                Console.WriteLine($"Exit with code: {process.ExitCode}");
            }


            XmlParser parser = new XmlParser(_dataFileFullPath);
            var customers = parser.Parse();
            var loader = new DataLoader(customers, threadPool, batchSize, attemptsSaveToDB);
            InitializeDb(connection);


            //loader.LoadData();
            await loader.LoadDataAsync();
        }

        static void GenerateCustomersDataFile()
        {
            var xmlGenerator = new XmlGenerator(_dataFileFullPath, _dataCount);
            xmlGenerator.Generate();
        }

        static void ParseArgs(string[] args)
        {
            if (args == null && args.Length == 0)
            {
                return;
            }
            if (args.Length == 1)
            {
                _dataFilePath = args[0];
                _dataFileFullPath = Path.Combine(_dataFilePath, "customers.xml");
            }
            if (args.Length == 2)
            {
                _dataFilePath = args[0];
                _generatorFilePath = args[1];
            }

        }

        static void InitializeDb(string connectionString)
        {
            using (var dataContext = new DataContext(connectionString))
            {
                new DbInitializer(dataContext).Initialize();
            }
        }

    }
}